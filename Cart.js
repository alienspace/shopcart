import React, { Component } from 'react';
import { Link } from 'react-router';

/** COMPONENTS CUSTOM **/
import { nextStorage } from '../../../util/nextStorage';
export class Cart extends Component{
  constructor(props){
    super(props);
    this.state = {

    }

    this.renderCart = this.renderCart.bind(this);
  }

  renderProductsList(){
    let nextDash = nextStorage.getJSON('nextDash');
    if(nextDash.CART_productsList.length > 0){
      return nextDash.CART_productsList.map((product) => {
        return (
          <li key={product.productID}>
            <span className={'title'}>{product.productTitle}</span>
            <span className={'qty'}>{product.productQty}x</span>
            <span className={'price'}>R$ {product.productPrice}</span>
          </li>
        );
      });
    }else{
      return (<li className={'empty'}>Nenhum produto na lista.</li>);
    }
  }//renderProductsList();

  renderCart(){
    if(this.props.cartListHover){
      return (
        <div className={'cartListHover'}>
          <ul>
            {this.renderProductsList()}
          </ul>
          <Link
            to={'/cart'}
            className={'btn btn-primary no-radius pull-right'}
            >
            <span>Finalizar</span>
          </Link>
        </div>
      );
    }
  }//renderCart();

  render(){
    return this.renderCart();
  }
}
